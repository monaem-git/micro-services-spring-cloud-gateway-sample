package org.opendevup;

import java.util.HashMap;
import java.util.Map;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.cloud.gateway.discovery.DiscoveryClientRouteDefinitionLocator;
import org.springframework.cloud.gateway.discovery.DiscoveryLocatorProperties;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableHystrix
public class GatewayServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayServiceApplication.class, args);
	}
	
	/*
	 * Static routes with urls
	 *
	 * @Bean RouteLocator staticRoutes(RouteLocatorBuilder builder) { return
	 * builder.routes()
	 * .route(r->r.path("/customers/**").uri("http://localhost:8081").id("r1"))
	 * .route(r->r.path("/products/**").uri("http://localhost:8082").id("r2"))
	 * .build(); }
	 */
	
	/*
	 * Static routes with Rergistry service
	 *
	 * @Bean RouteLocator staticRoutes(RouteLocatorBuilder builder) { return
	 * builder.routes()
	 * .route(r->r.path("/customers/**").uri("lb://CUSTOMER-SERVICE").id("r1"))
	 * .route(r->r.path("/products/**").uri("lb://INVENTORY-SERVICE").id("r2"))
	 * .build(); }
	 */
	
	@Bean 
	RouteLocator staticRoutes(RouteLocatorBuilder builder) { 
		return
			  builder.routes()
			  .route(r->r	
					  .path("/publicCountries/**")
					  .filters(f->f
							  .addRequestHeader("x-rapidapi-host", "restcountries-v1.p.rapidapi.com")
							  .addRequestHeader("x-rapidapi-key", "f0287b2584msh2c55eea943a6142p1c868djsn3729c8043452")
							  .rewritePath("/publicCountries/(?<segment>.*)", "/${segment}")
							  .hystrix(h->h.setName("countries").setFallbackUri("forward:/defaultCountries"))
							  )
					  .uri("https://restcountries-v1.p.rapidapi.com")
					  .id("r1"))
			  .route(r->r	
					  .path("/muslimSalat/**")
					  .filters(f->f
							  .addRequestHeader("x-rapidapi-host", "muslimsalat.p.rapidapi.com")
							  .addRequestHeader("x-rapidapi-key", "f0287b2584msh2c55eea943a6142p1c868djsn3729c8043452")
							  .rewritePath("/muslimSalat/(?<segment>.*)", "/${segment}")
							  )
					  .uri("https://muslimsalat.p.rapidapi.com")
					  .id("r2"))
			  
			  .build(); 
		}
	// Routage Dynamique ( à partir de url http://localhost:8888/INVENTORY-SERVICE/products/ )
	@Bean
	DiscoveryClientRouteDefinitionLocator dynamicRoutes(ReactiveDiscoveryClient discoveryClient, DiscoveryLocatorProperties properties) {
		return new DiscoveryClientRouteDefinitionLocator(discoveryClient, properties);
	}
	
}

@RestController
class CircuitBreakerRestControlle {
	@GetMapping("/defaultCountries")
	public Map<String,String> defaultCountries() {
		Map<String,String> data = new HashMap<String, String>();
		data.put("message", "default countries");
		data.put("countries", "Maroc, Algérie, Tunisie");
		return data; 
	}
}
